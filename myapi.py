from flask import Flask, request
import json
import math

#inicialização do objeto utilizando o Flask framework
app = Flask(__name__)

#cria uma variável que contém um JSON inicial para a aplicação
s = '{"Fact":"0", "Fib":"0"}'
di = json.loads(s)

#definição da rota da api e métodos aceitos
#recebe um JSON com as chaves "Fact" e "Fib" e valores inteiros
#retorna o fatorial e o correspondente da sequência de fibnoacci dos valores
@app.route('/myapi', methods=['POST'])
def myapi():
    di = request.get_json()
    di["Fact"] = math.factorial(int(di["Fact"]))
    di["Fib"] = fibonacci(int(di["Fib"]))
    return di

#definição da função para calcular a sequeência de fibonacci
def fibonacci(n):
    n = int(n)
    fib = [0, 1]
    if n < 0:
        raise ValueError

    elif n == 0:
        return fib[0]
    
    elif n == 1:
        return fib[1]
    
    else:
        for i in range(1, n):
            fib.append(fib[i] + fib[i-1])
        
        return fib[n-1]
         


