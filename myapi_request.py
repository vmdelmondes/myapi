import requests
import json

# Make the POST request

#url - localhost:porta/endpoint
api_url = "http://127.0.0.1:5000/myapi"

#prepara o json para fazer o request
data = json.loads('{"Fact":"6", "Fib":"8"}')

#utiliza o método POST para enviar dados ao servidor e salva a resposta
response = requests.post(api_url, json=data)

#print da resposta no terminal
#print do status code. Diz se o pedido foi bem sucedido e ajuda a entender o problema caso ocorra
print("Response Status Code:", response.status_code)
print("Response JSON:", response.json())
